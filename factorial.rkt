#lang racket

; Implementando la función factorial

(define (is-negative? number)
  (if (< number 0) #t #f))


(define (factorialAux number result)
  (cond
    [(equal? number 0) result]
    [else (factorialAux (sub1 number) (* result number))]))

(define (factorial number)
  (cond
    [(is-negative? number) (error  "No se puede calcular el factorial de un número negativo")]
    [(equal? number 0) 1]
    [else (factorialAux number 1)]))

;(factorial -1)