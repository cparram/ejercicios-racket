#lang racket

; Retorna el número de fibonacci que está en cierta position
; (fibonacci-number 0) => 1
; (fibonacci-number 1) => 1
; (fibonacci-number 2) => 2

(define (fibonacci-number at)
  (define (iter index prior previous position)
    (cond
      [(equal? position index) prior]
      [else (iter (add1 index) previous (+ prior previous) position)]))
  (iter 0 1 1 at))


;Otra implementación de los números de fibonacci
;definiendo parámetros por default
;(fibonacci)              =>1 
;(fibonacci #:position 1) =>1
;(fibonacci #:position 2) =>2
;(fibonacci #:position 3) =>3
;(fibonacci #:position 4) =>5

(define fibonacci
  (lambda ( #:position [at 0]
                       #:index [index 0]
                       #:prior [prior 1]
                       #:previous [previous 1])
    (cond
      [(equal? at index) prior]
      (else (fibonacci #:position at 
                       #:index (add1 index) 
                       #:prior previous 
                       #:previous (+ prior previous))))))



;Procedimiento que permiter obtener un rango de números de fibonacci
;(fibonacci-number-range #:from 0 #:to 4) => '(1 1 2 3 5)

(define fibonacci-number-range 
  (lambda (#:from [from 0] 
                  #:to [to 1]
                  #:list [fibonacci-list empty])
    (cond 
      [(equal? from (add1 to)) (reverse fibonacci-list)]
      [else (fibonacci-number-range #:from (add1 from)
                                    #:to to
                                    #:list (cons (fibonacci #:position from)
                                                 fibonacci-list))])))
                  
