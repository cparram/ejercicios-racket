#lang scheme

;Asumiendo que el archivo de entrada tiene el 
;formato establecido

(define filename "crown2.txt")

;Lee la cantidad de colores y nodos. 
(define numbers-colors/nodes
  (lambda (#:name-file [file filename])
    (define input-file (open-input-file file))
    (list input-file (read input-file) (read input-file))))

;Lee los colores dados en el archivo
(define read-colors 
  (lambda (#:input-file [input (car (numbers-colors/nodes))]
                        #:color-amount [amount (cadr (numbers-colors/nodes))])
    (define (reading-colors start end lst)
      (cond
        [(= start end) (reverse lst)]
        [else (reading-colors (add1 start) end (cons (read input) lst))]))
    (list input (reading-colors 0 amount empty))))

;(numbers-colors/nodes)    
;(read-colors)

;Lee la matris de adyacencia
(define read-adjacency-matris 
  (lambda (#:input-file [input (car (read-colors))]
                       #:nodes-amount [amount  (caddr (numbers-colors/nodes))])
    (define (read-row start end lst)
      (cond
        [(= start end) (reverse lst)]
        [(equal? (read input) 1) (read-row (add1 start) end (cons start lst))]
        [else (read-row (add1 start) end lst)]))
    (define (read-columns start end lst)
      (cond
        [(= start end) (reverse lst)]
        [else (read-columns (add1 start) end 
                            (cons (read-row 0 amount empty) lst))]))
    (read-columns 0 amount empty)))

;Lee la cantidad de aristas
(define read-aristas 
  (lambda (#:input-file [input (car (read-colors))]
                       #:nodes-amount [amount  (caddr (numbers-colors/nodes))])
    (define (read-row iter-x start end lst)
      (cond
        [(= start end) (reverse lst)]
        [(and (equal? (read input) 1) (<= iter-x start)) (read-row iter-x (add1 start) end (cons start lst))]
        [else (read-row iter-x (add1 start) end lst)]))
    (define (read-columns start end lst)
      (cond
        [(= start end) (reverse lst)]
        [else (read-columns (add1 start) end 
                            (cons (read-row start 0 amount empty) lst))]))
    (read-columns 0 amount empty)))

;Cantidad de aristas de la matris
(define numbers-aristas
  (lambda (#:aristas-readed [aristas (read-aristas)])
  (define (counter aristas iter rslt)
    (define next-iter (add1 iter))
    (cond 
      [(= next-iter (length aristas)) rslt]
      [else (counter aristas next-iter 
                     (+ rslt (length(list-ref aristas next-iter))))]))
  (counter aristas -1 0)))

;Cantidad de grados que tiene cada nodo en la matris
(define node-degrees
  (lambda (#:matris-readed [matris (read-adjacency-matris)])
  (define (degrees matris iter rslt)
    (define next-iter (add1 iter))
    (cond 
      [(= next-iter (length matris)) (reverse rslt)]
      [else (degrees matris next-iter 
                     (cons (length (list-ref matris next-iter)) rslt))]))
    (degrees matris -1 empty)))
    
    
(define matris (read-adjacency-matris))
(define aristas (read-aristas))
(define aristas-amount (numbers-aristas #:aristas-readed aristas))
(define degrees (node-degrees #:matris-readed matris))
(define nodes-amount (caddr (numbers-colors/nodes)))  

(printf "matris:   ") matris 
(printf "aristas   ") aristas 
(printf "#aristas: ") aristas-amount 
(printf "degrees:  ") degrees 
(printf "#nodes:   ") nodes-amount

(define degree-max (apply max degrees))
(define degree-min (apply min degrees))

;Si un grafo es complete #t de lo contrario #f
(define is-complete-graph
  (lambda (#:aristas-amount [edges aristas-amount] #:nodes-amount [nodes nodes-amount])
    (cond
      [(= edges (/ (* nodes (sub1 nodes)) 2)) #t]
      [else #f])))

(printf "min-number of colors: ")
(cond
  [(= degree-max 0) 1]
  [(= nodes-amount aristas-amount) (if (= (remainder nodes-amount 2) 0) (/ nodes-amount 2) (add1 (- (/ nodes-amount 2) .5)))]
  [(= degree-max aristas-amount) 2]
  [(= (remainder nodes-amount 2) 0) (if (is-complete-graph) (add1 degree-max) degree-max)]
  [else degree-max])
     